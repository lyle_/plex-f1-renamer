package main

import (
	"github.com/go-test/deep"
	"os"
	"path"
	"testing"
	"time"
)

func Test_loadConfig(t *testing.T) {
	deep.CompareUnexportedFields = true
	type env struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		env     []env
		want    *config
		wantErr bool
	}{
		{
			name:    "No variables set results in error",
			env:     []env{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "All variables set results in a valid configuration",
			env: []env{
				{key: "TORRENT_DIRECTORY", value: "/torrent_dir"},
				{key: "TARGET_DIRECTORY", value: "/target_dir"},
			},
			want: &config{torrentDirectory: "/torrent_dir", targetDirectory: "/target_dir"},
		},
		{
			name: "Missing TORRENT_DIRECTORY results in error",
			env: []env{
				{key: "TARGET_DIRECTORY", value: "/target_dir"},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Missing TARGET_DIRECTORY results in error",
			env: []env{
				{key: "TORRENT_DIRECTORY", value: "/torrent_dir"},
			},
			want:    nil,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set environment variables
			for _, env := range tt.env {
				os.Setenv(env.key, env.value)
				defer unset(env.key)
			}

			got, err := loadConfig()
			if tt.wantErr && err == nil {
				t.Error("Expecting error, got none")
			} else if !tt.wantErr && err != nil {
				t.Error(err)
			}

			diff := deep.Equal(got, tt.want)
			if diff != nil {
				t.Errorf("loadConfig() %#v", diff)
			}
		})
	}
}

func Test_parseRoundTitle(t *testing.T) {
	tests := []struct {
		name    string
		title   string
		want    *round
		wantErr bool
	}{
		{
			name:    "Empty title",
			title:   "",
			wantErr: true,
		},
		{
			name:    "Non-conforming title",
			title:   "Random reddit post that doesn't match our expected format",
			wantErr: true,
		},
		{
			name:  "Correct title result in complete round struct",
			title: "F1.2023.Round.20.Mexican.Weekend.SkyF1.1080P",
			want:  &round{Number: 20, Title: "Mexican GP", Year: 2023},
		},
		{
			name:  "Title with 'R01' instead of 'Round.1'",
			title: "F1.2024.R01.Bahrain.Grand.Prix.SkyF1HD.1080P",
			want:  &round{Number: 1, Title: "Bahrain GP", Year: 2024},
		},
		{
			name:  "F1 Preseason testing",
			title: "F1 2019. Preseason Testing. Sky Sports F1 HD. 1080P",
			want:  &round{Year: 2019, Number: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "F1. Preseason testing",
			title: "F1. 2021. Preseason Testing. Sky Sports F1 HD",
			want:  &round{Year: 2021, Number: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "F1. ... Grand Prix suffix",
			title: "F1. 2021. R18. Mexican Grand Prix. Weekend On Sky F1 HD. 1080P",
			want:  &round{Year: 2021, Number: 18, Title: "Mexican GP"},
		},
		{
			name:  "F1. ... Testing",
			title: "F1. 2022. Testing. Bahrain. SkyF1HD. 1080P",
			want:  &round{Year: 2022, Number: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "Formula 1. ... Weekend suffix",
			title: "Formula 1. 2022. R22. Abu Dhabi Weekend. SkyF1HD. 1080P",
			want:  &round{Year: 2022, Number: 22, Title: "Abu Dhabi GP"},
		},
		{
			name:  "Formula 1. ... Pre Season Testing",
			title: "Formula 1. 2023. Pre Season Testing. Bahrain. SkyF1HD. 1080P",
			want:  &round{Year: 2023, Number: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "F1, Testing, no Weekend",
			title: "F1.2024.Testing.Bahrain.SkyF1HD.1080P",
			want:  &round{Year: 2024, Number: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "Formula 1. ... no GP/Weekend suffix",
			title: "Formula 1. 2023. R01. Bahrain. SkyF1HD. 1080P",
			want:  &round{Year: 2023, Number: 1, Title: "Bahrain GP"},
		},
		{
			name:  "Formula 1., dots and spaces, no GP/Weekend suffix",
			title: "Formula 1. 2023. R02. Saudi Arabian. SkyF1HD. 1080P",
			want:  &round{Year: 2023, Number: 2, Title: "Saudi Arabian GP"},
		},
		{
			name:  "F1, dots no spaces, Weekend suffix",
			title: "F1.2023.Round.23.Abu.Dhabi.Weekend.SkyF1.1080P",
			want:  &round{Year: 2023, Number: 23, Title: "Abu Dhabi GP"},
		},
		{
			name:  "Complete round parsing",
			title: "Formula 1. 2023. R02. Saudi Arabian. SkyF1HD. 1080P",
			want:  &round{Year: 2023, Number: 2, Title: "Saudi Arabian GP"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseRoundTitle(tt.title)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseRoundTitle() error = %v, wantErr %v", err, tt.wantErr)
				return
			} else if err == nil {
				diff := deep.Equal(got, tt.want)
				if diff != nil {
					t.Errorf("parseRoundTitle(%s) %#v", tt.title, diff)
				}
			}
		})
	}
}

func Test_parseFilename(t *testing.T) {
	tmpdir := t.TempDir()
	tests := []struct {
		name     string
		filename string
		want     *session
		wantErr  bool
	}{
		{
			name:     "Empty filename returns an error",
			filename: "",
			wantErr:  true,
		},
		{
			name:     "Press Conference",
			filename: "01.F1.2023.Round.20.Mexican.Drivers.Press.Conference.SkyF1HD.1080P.mkv",
			want:     &session{Number: 1, Title: "Drivers Press Conference"},
		},
		{
			name:     "Free Practice 1",
			filename: "02.F1.2023.Round.02.Saudi.Arabian.Free.Practice.One.SkyF1HD.1080P.mkv",
			want:     &session{Number: 2, Title: "Free Practice One"},
		},
		{
			name:     "Free Practice 2",
			filename: "03.F1.2023.Round.06.Monaco.Free.Practice.Two.SkyF1HD.1080P.mkv",
			want:     &session{Number: 3, Title: "Free Practice Two"},
		},
		{
			name:     "F1 Show",
			filename: "04.F1.2023.Round.15.Italian.F1.Show.SkyF1HD.1080P.mkv",
			want:     &session{Number: 4, Title: "F1 Show"},
		},
		{
			name: "Free Practice 3",
			// The publisher missed the period after Round
			filename: "05.F1.2023.Round08.Spanish.Free.Practice.Three.SkyF1HD.1080.mkv",
			want:     &session{Number: 5, Title: "Free Practice Three"},
		},
		{
			name:     "Qualifying",
			filename: "06.F1.2023.Round.02.Saudi.Arabian.Qualifying.SkyF1HD.1080P.mkv",
			want:     &session{Number: 6, Title: "Qualifying"},
		},
		{
			name:     "Ted's Qualifying Notebook",
			filename: "07.F1.2023.Round.09.Canadian.Teds.Qualifying.Notebook.SkyF1HD.1080P.mkv",
			want:     &session{Number: 7, Title: "Ted's Qualifying Notebook"},
		},
		{
			// SkyF1 had camera issues during the live notebook and rebroadcast a fixed version
			name:     "Ted's Qualifying Notebook Replay",
			filename: "04b.F1.2023.Round.21.Sao.Paulo.Teds.Qualifying.Notebook.Replay.SkyF1HD.1080P.mkv",
			want:     &session{Number: 4, Title: "Ted's Qualifying Notebook Replay"},
		},
		{
			name:     "Race",
			filename: "08.F1.2023.Round.13.Belgian.Race.SkyF1HD.1080P.mkv",
			want:     &session{Number: 8, Title: "Race"},
		},
		{
			name:     "Ted's Notebook",
			filename: "09.F1.2023.Round.19.United.States.Teds.Notebook.SkyF1HD.1080P.mkv",
			want:     &session{Number: 9, Title: "Ted's Notebook"},
		},
		{
			name:     "F1 Juniors",
			filename: "10.F1.2023.Round.12.Hungarian.F1.Juniors.Sky.Sports.Mix.HD.1080P.mkv",
			want:     &session{Number: 10, Title: "F1 Juniors"},
		},
		{
			name:     "Sprint Shootout",
			filename: "05.F1.2023.Round.10.Austrian.Sprint.Shootout.SkyF1HD.1080P.mkv",
			want:     &session{Number: 5, Title: "Sprint Shootout"},
		},
		{
			name:     "Sprint",
			filename: "06.F1.2023.Round.18.Qatar.Sprint.SkyF1HD.1080P.mkv",
			want:     &session{Number: 6, Title: "Sprint"},
		},
		{
			name:     "Ted's Sprint Notebook",
			filename: "07.F1.2023.Round.21.Sao.Paulo.Teds.Sprint.Notebook.SkyF1HD.1080P.mkv",
			want:     &session{Number: 7, Title: "Ted's Sprint Notebook"},
		},
		{
			name:     "R instead of Round",
			filename: "07.F1.2024.R02.Saudi.Arabian.Race.SkyF1HD.1080P.mkv",
			want:     &session{Number: 7, Title: "Race"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Specify this here instead of repeating for each test
			if tt.want != nil {
				tt.want.FilePath = path.Join(tmpdir, tt.filename)
			}

			got, err := parseFilename(tmpdir, tt.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseFilename() error = %v, wantErr %v", err, tt.wantErr)
				return
			} else if err == nil {
				diff := deep.Equal(got, tt.want)
				if diff != nil {
					t.Errorf("parseFilename(%s) %#v", tt.filename, diff)
				}
			}
		})
	}
}

func Test_watchForChanges(t *testing.T) {
	type fileCreationScenario int
	const (
		allPreexisting fileCreationScenario = iota
		// Both the test Round and child Session are created after startup
		nonePreexisting
		// The test Round exists before startup, child Session created after
		roundPreexistsSessionAddedAfter
	)
	type args struct {
		round    *round
		roundDir string
		session  *session
		wantPath string
	}
	defaultArgs := &args{
		round:    &round{Number: 4, Title: "Bahrain GP", Year: 2023},
		roundDir: "F1.2023.Round.04.Bahrain.Weekend.SkyF1.1080P",
		session:  &session{FilePath: "05.F1.2023.Round.04.Bahrain.Qualifying.SkyF1HD.1080p.mkv", Number: 5, Title: "Qualifying"},
		wantPath: "Season 2023/04 - Bahrain GP/04x05 - Bahrain GP - Qualifying.mkv",
	}
	tests := []struct {
		name     string
		args     args
		scenario fileCreationScenario
	}{
		{
			name:     "Round dir and session file exist before startup",
			args:     *defaultArgs,
			scenario: allPreexisting,
		},
		{
			name:     "New round dir and session file both appear after startup",
			args:     *defaultArgs,
			scenario: nonePreexisting,
		},
		{
			name:     "Pre-existing Round is watched after startup",
			args:     *defaultArgs,
			scenario: roundPreexistsSessionAddedAfter,
		},
	}
	for _, tt := range tests {
		tmpdir := t.TempDir()
		t.Run(tt.name, func(t *testing.T) {
			// Create a download directory for test torrent files
			torrentDirectory := path.Join(tmpdir, "torrentDirectory")
			err := os.Mkdir(torrentDirectory, 0755)
			if err != nil {
				t.Errorf("creating test directory for torrent files: %v", err)
			}
			// Create a target directory for renamed/hardlinked rounds
			targetDirectory := path.Join(tmpdir, "targetDirectory")
			err = os.Mkdir(targetDirectory, 0755)
			if err != nil {
				t.Errorf("creating test directory for target files: %v", err)
			}

			// Set environment variables
			os.Setenv("TORRENT_DIRECTORY", torrentDirectory)
			defer unset("TORRENT_DIRECTORY")
			os.Setenv("TARGET_DIRECTORY", targetDirectory)
			defer unset("TARGET_DIRECTORY")

			roundPath := path.Join(torrentDirectory, tt.args.roundDir)
			switch tt.scenario {
			case allPreexisting:
				// The round directory and session file are created before fsnotify watch
				createTorrentDirectory(path.Join(torrentDirectory, tt.args.roundDir), t)
				createTorrentFile(path.Join(roundPath, tt.args.session.FilePath), t)
				runMain()
				break
			case nonePreexisting:
				// The round directory and session file are created after fsnotify watch
				runMain()
				createTorrentDirectory(path.Join(torrentDirectory, tt.args.roundDir), t)
				createTorrentFile(path.Join(roundPath, tt.args.session.FilePath), t)
				break
			case roundPreexistsSessionAddedAfter:
				// The round directory exists but the session file is created after fsnotify watch
				createTorrentDirectory(roundPath, t)
				runMain()
				createTorrentFile(path.Join(roundPath, tt.args.session.FilePath), t)
				break
			default:
				t.Errorf("Unknown scenario (%d)", tt.scenario)
			}

			// Allow time for filesystem event to propagate
			time.Sleep(1 * time.Second)

			// Check that the renamed file exists
			wantFile := path.Join(targetDirectory, tt.args.wantPath)
			if _, err := os.Stat(wantFile); err != nil {
				t.Errorf("expected file '%s' does not exist", wantFile)
			}
		})
	}
}

// createTorrentDirectory creates a directory for a new torrent
func createTorrentDirectory(path string, t *testing.T) {
	err := os.Mkdir(path, 0755)
	if err != nil {
		t.Errorf("creating test torrent round directory %s: %v", path, err)
	}
}

// createTorrentFile creates a new file within a torrent directory
func createTorrentFile(path string, t *testing.T) {
	_, err := os.Create(path)
	if err != nil {
		t.Errorf("creating test torrent file for session %s: %v", path, err)
	}
}

func runMain() {
	// Start watching for file changes
	go main()
	// Allow time for watcher to be created
	time.Sleep(1 * time.Second)
}

func unset(key string) {
	os.Unsetenv(key)
}
