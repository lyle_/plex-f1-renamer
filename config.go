package main

import (
	"errors"
	"fmt"
	"go.uber.org/zap"
	"os"
)

type config struct {
	torrentDirectory string
	targetDirectory  string
	logLevel         string
}

func loadConfig() (*config, error) {
	appConfig := &config{}
	if value, exists := os.LookupEnv("TORRENT_DIRECTORY"); exists {
		appConfig.torrentDirectory = value
	} else {
		return nil, errors.New("TORRENT_DIRECTORY is not set, expecting it to specify location of completed torrent")
	}
	if value, exists := os.LookupEnv("TARGET_DIRECTORY"); exists {
		appConfig.targetDirectory = value
	} else {
		return nil, errors.New("TARGET_DIRECTORY is not set, expecting it to specify the desired Plex library path")
	}
	if value, exists := os.LookupEnv("LOG_LEVEL"); exists {
		appConfig.logLevel = value
	}
	fmt.Printf("Loaded config: %#v\n", appConfig)
	return appConfig, nil
}

// configureLogging sets up the logger according to configured preferences
func configureLogging(config config) *zap.SugaredLogger {
	level, err := zap.ParseAtomicLevel(config.logLevel)
	if err != nil {
		fmt.Printf("Error parsing desired log level, defaulting to 'info': %v\n", err)
		level = zap.NewAtomicLevelAt(zap.InfoLevel)
	}

	c := zap.NewDevelopmentConfig()
	c.Level = level
	logger, err := c.Build()
	if err != nil {
		panic(err)
	}

	return logger.Sugar()
}
