# Plex F1 Renamer

Plex F1 Renamer examines your F1 download directory and copies (or [hardlinks](https://trash-guides.info/Hardlinks/Hardlinks-and-Instant-Moves/)) Formula 1 files to a target directory for ingestion and proper display in a [Plex](https://www.plex.tv/) library.
It scans your entire torrent directory on startup, and watches for any new files to appear thereafter.

## Torrent file naming conventions
The torrent file naming convention is based upon conventions used by Reddit user [u/egortech](https://www.reddit.com/user/egortech/) since they are currently a very reliable uploader who names posts relatively consistently.
These names can vary somewhat and I do my best to interpret them, but feel free to [file a bug report](https://gitlab.com/lyle_/plex-f1-renamer/-/issues) if there are files that aren't parsed correctly.

## Plex naming conventions
The output naming convention is the one [specified by Kometa](https://metamanager.wiki/en/latest/kometa/guides/formula/), a metadata manager for Plex.

For example:
```
Formula 1                             -> Library Folder
└── Season 2018                       -> Folder for each F1 Season
├── 01 - Australian GP                -> Folder for each Race in a season
│   ├── 01x10 - Australian GP - Highlights.mkv
│   ├── 01x01 - Australian GP - Free Practice 1.mkv
```

## Configuration

The app expects these environment variables to be set:

| Environment variable | Default | Description |
| -------------------- | ------- | ----------- |
| TORRENT_DIRECTORY | | Location where race new weekends are downloaded |
| TARGET_DIRECTORY | | The top-level directory of your Formula 1 library in Plex |
| LOG_LEVEL | `info` | Log level string. `debug`, `info`, `warn`, or `error` |
| PUID | | User ID to run as |
| PGID | | Group ID to run as |

## Related Projects

* [reddit-f1-scanner](https://gitlab.com/lyle_/reddit-f1-scanner) - Searches for Formula 1 torrents posted on [r/MotorsportsReplays](https://www.reddit.com/r/MotorsportsReplays/) and sends them to a Transmission daemon for downloading.
