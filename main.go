package main

import (
	"errors"
	"fmt"
	"go.uber.org/zap"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/fsnotify/fsnotify"
)

// round is a Formula 1 event
type round struct {
	Number int    // 0 for preseason testing, regular numbering for race weekends
	Title  string // "Bahrain", "Saudi Arabian". Read as implicitly followed by "Grand Prix" or "GP"
	Year   int    // 2023
}

// session is an individual event within a round; free practice, qualifying, race, etc.
type session struct {
	FilePath string
	Number   int
	Title    string
}

func main() {
	appConfig, err := loadConfig()
	if err != nil {
		panic(err)
	}
	log := configureLogging(*appConfig)
	watchForChanges(appConfig.torrentDirectory, appConfig.targetDirectory, log)
}

// watchForChanges monitors torrentDirectory for new files and hardlinks (or copies) them
// into a prettified library structure in targetDirectory.
func watchForChanges(torrentDirectory, targetDirectory string, log *zap.SugaredLogger) {
	// Create new file system watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}
	defer watcher.Close()

	// Add the torrent download path
	log.Infof("Watching torrent directory %s", torrentDirectory)
	err = watcher.Add(torrentDirectory)
	if err != nil {
		panic(err)
	}

	// Add watches to any existing round folders within the torrent download path
	dirs, err := childDirectories(torrentDirectory)
	if err != nil {
		panic(err)
	}
	for _, dir := range dirs {
		log.Infof("Watching existing round directory %s", torrentDirectory)
		err = watcher.Add(dir)
		if err != nil {
			panic(err)
		}
	}

	// Scan all existing torrents of F1 rounds to do a full sync of the Plex library
	log.Infof("Doing a full scan of existing rounds in %s", torrentDirectory)
	dirs, err = childDirectories(torrentDirectory)
	for _, dir := range dirs {
		log.Infof("Watching existing round directory %s", torrentDirectory)
		scanRoundContents(dir, targetDirectory, log)
		if err != nil {
			panic(err)
		}
	}

	// Start listening for events
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					log.Error("Error creating fsnotify channel")
					return
				}
				if event.Has(fsnotify.Create) {
					isDir, err := pathIsDirectory(event.Name)
					if err != nil {
						log.Errorf("Error determining if '%s' is a directory: %s", event.Name, err)
						continue
					}
					if isDir {
						log.Infof("Watching new round directory %s", event.Name)
						err := watcher.Add(event.Name)
						if err != nil {
							log.Errorf("Error adding a watch on round directory %s: %s", event.Name, err)
						}
						// Proactively scan round directory since files might have been created before our new watcher
						scanRoundContents(event.Name, targetDirectory, log)
					} else {
						// Assume we've spotted a new session
						log.Infof("New file detected %s", event.Name)
						scanRoundContents(path.Dir(event.Name), targetDirectory, log)
					}
				} else {
					log.Debugf("Not handling event %v", event)
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Errorf("fsnotify watcher error: %s:", err)
			}
		}
	}()

	// Block main goroutine forever.
	<-make(chan struct{})
}

// scanRoundContents examines roundPath for any new session files and links them into targetDirectory
func scanRoundContents(roundPath string, targetDirectory string, log *zap.SugaredLogger) {
	log.Debugf("Scanning torrent Round directory %s", roundPath)

	// Parse torrent name from path
	pathElements := strings.Split(roundPath, "/")
	torrentName := pathElements[len(pathElements)-1]

	// Parse torrent name into F1 Weekend
	round, err := parseRoundTitle(torrentName)
	if err != nil {
		log.Errorf("Error parsing round title: %s", err)
		return
	}

	// Parse the files in the torrent directory
	roundFiles, err := os.ReadDir(roundPath)
	if err != nil {
		log.Errorf("Error reading roundPath '%s': %s", roundPath, err)
		return
	}
	var sessions []*session
	for _, sessionFile := range roundFiles {
		session, err := parseFilename(roundPath, sessionFile.Name())
		if err != nil {
			log.Errorf("%s", err)
		} else {
			sessions = append(sessions, session)
		}
	}

	if len(sessions) > 0 {
		err = copyRoundFiles(targetDirectory, round, sessions, log)
		if err != nil {
			log.Errorf("Error copying round files: %s", err)
		}
	} else {
		log.Debug("Empty directory")
	}
}

// childDirectories returns the path of every immediate child directory of parentPath
func childDirectories(parentPath string) ([]string, error) {
	var dirs []string

	entries, err := os.ReadDir(parentPath)
	if err != nil {
		return nil, err
	}

	for _, entry := range entries {
		childPath := path.Join(parentPath, entry.Name())
		isDir, err := pathIsDirectory(childPath)
		if err != nil {
			return nil, fmt.Errorf("error determining if child path is a directory: %w", err)
		}
		if isDir == true {
			dirs = append(dirs, path.Join(childPath))
		}
	}

	return dirs, nil
}

// pathIsDirectory returns a boolean indicating whether the given path is a directory
func pathIsDirectory(path string) (bool, error) {
	file, err := os.Open(path)
	if err != nil {
		return false, fmt.Errorf("opening path: %w", err)
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return false, fmt.Errorf("calling file.Stat(): %w", err)
	}
	return fileInfo.IsDir(), nil
}

// parseRoundTitle takes a torrent title and returns a round
func parseRoundTitle(title string) (*round, error) {
	round := round{}

	// Example title: F1.2023.Round.20.Mexican.Weekend.SkyF1.1080P
	// Regex can be tested at https://regexr.com/7nth4
	re := `(?i)(?:Formula 1|F1)[\. ]+(?P<year>\d{4})[\. ]+(?P<number>(R\d{2}|Round\.\d{2}|.*testing))[\. ]+(?P<title>.*)(?:Sky)`
	r := regexp.MustCompile(re)
	matches := r.FindStringSubmatch(title)
	if len(matches) == 0 {
		return &round, fmt.Errorf("parsing title '%s'; got %d matches", title, len(matches))
	}

	year, err := strconv.Atoi(matches[1])
	if err != nil {
		return &round, fmt.Errorf("parsing year from title '%s': %w", title, err)
	}
	round.Year = year

	if strings.Contains(strings.ToLower(matches[2]), "testing") {
		round.Number = 0
		round.Title = "Pre-Season Testing"
	} else {
		// Trim the leading characters from "R01" or "Round.01" and convert to int
		index := strings.IndexAny(matches[2], "0123456789")
		if index < 0 {
			return &round, fmt.Errorf("no digit found in round substring '%s'", matches[2])
		}
		number, err := strconv.Atoi(matches[2][index:])
		if err != nil {
			return &round, fmt.Errorf("parsing round number from title '%s': %w", title, err)
		}
		round.Number = number

		round.Title = matches[4]

		// There's probably something I can do with the current regex to capture titles perfectly, but as it is we sometimes
		// see "Abu Dhabi Weekend" or "Abu Dhabi Grand Prix". I'd love to figure out the ideal regex, but given the difficulty
		// I'm having I'll concede for now with some manual trimming.
		round.Title = strings.ReplaceAll(round.Title, ".", " ")
		index = strings.Index(round.Title, "Weekend")
		if index >= 0 {
			round.Title = strings.TrimSuffix(round.Title, round.Title[index:])
		}
		index = strings.Index(round.Title, "Grand Prix")
		if index >= 0 {
			round.Title = strings.TrimSuffix(round.Title, round.Title[index:])
		}
		round.Title = strings.TrimSpace(round.Title) + " GP"
	}

	return &round, nil
}

// parseFilename takes the path of an individual file and returns a session
func parseFilename(directory string, filename string) (*session, error) {
	// Copied from https://github.com/meisnate12/Plex-Meta-Manager/blob/bd7bbc6ac1ed60e7f81f479c0b2ff906624d12b1/modules/ergast.py#L9
	placeNames := []string{
		"70th Anniversary", "Abu Dhabi", "Argentine", "Australian", "Austrian", "Azerbaijan", "Bahrain", "Belgian",
		"Brazilian", "British", "Caesars Palace", "Canadian", "Chinese", "Dallas", "Detroit", "Dutch", "Eifel",
		"Emilia Romagna", "European", "French", "German", "Hungarian", "Indian", "Indianapolis 500", "Italian",
		"Japanese", "Korean", "Luxembourg", "Malaysian", "Mexican", "Mexico City", "Miami", "Monaco", "Moroccan",
		"Pacific", "Pescara", "Portuguese", "Portugal", "Qatar", "Russian", "Sakhir", "San Marino", "Saudi Arabian",
		"Singapore", "South African", "Spanish", "Styrian", "Swedish", "Swiss",
		"São Paulo", "Sao Paulo", "Turkish", "Tuscan", "United States",
	}

	session := session{FilePath: path.Join(directory, filename)}

	// Example filename: 01.F1.2023.Round.20.Mexican.Drivers.Press.Conference.SkyF1HD.1080P.mkv
	re := `(?i)(?P<number>\d{2})[[:alpha:]]?\.?(?:Formula 1|F1)\.?(?:\d{4})\.(?:Round|R)[\.]?(?:\d{2})\.(?P<title>.*)(?:Sky)`
	r := regexp.MustCompile(re)
	matches := r.FindStringSubmatch(filename)
	if len(matches) != 3 {
		return &session, fmt.Errorf("parsing filename '%s'; got %d matches", filename, len(matches))
	}

	number, err := strconv.Atoi(matches[1])
	if err != nil {
		return &session, fmt.Errorf("parsing session number from filename '%s': %w", filename, err)
	}
	session.Number = number

	// Clean up session title string
	session.Title = strings.ReplaceAll(matches[2], ".", " ")
	// Remove any place names from session title
	for _, name := range placeNames {
		session.Title = strings.Replace(session.Title, name, "", 1)
	}
	session.Title = strings.Trim(session.Title, " ")
	session.Title = strings.Replace(session.Title, "Teds", "Ted's", 1)

	return &session, nil
}

// copyRoundFiles creates a hard link in targetDirectory for each provided session. If it encounters errors, it
// will continue trying to copy sessions and will return the first error encountered.
func copyRoundFiles(targetDirectory string, round *round, sessions []*session, log *zap.SugaredLogger) error {
	// Ensure the Round directory exists
	seasonDirectory := path.Join(targetDirectory, fmt.Sprintf("Season %d", round.Year))
	roundDirectory := path.Join(seasonDirectory, fmt.Sprintf("%02d - %s", round.Number, round.Title))

	if _, err := os.Stat(roundDirectory); os.IsNotExist(err) {
		// Round directory does not exist, create it
		if err := os.MkdirAll(roundDirectory, 0755); err != nil {
			return fmt.Errorf("error creating Round directory %s: %w", roundDirectory, err)
		}
		log.Infof("Created target Round directory: %s", roundDirectory)
	}

	var firstError error
	for _, session := range sessions {
		newFilename := fmt.Sprintf("%02dx%02d - %s - %s%s", round.Number, session.Number, round.Title, session.Title, filepath.Ext(session.FilePath))
		toPath := path.Join(roundDirectory, newFilename)
		if err := os.Link(session.FilePath, toPath); err == nil {
			log.Infof("Hardlinked from '%s' to '%s'", session.FilePath, toPath)
		} else if !errors.Is(err, os.ErrExist) {
			if firstError == nil {
				firstError = err
			}
			log.Errorf("Error linking file: %s", err)
		}
	}
	return firstError
}
