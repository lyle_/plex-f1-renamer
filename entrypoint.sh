#!/bin/bash

RUN_AS=abc
if [ ! "$(id -u ${RUN_AS})" -eq "$PUID" ]; then
    usermod -o -u "$PUID" ${RUN_AS};
fi
if [ -n "$PGID" ] && [ ! "$(id -g ${RUN_AS})" -eq "$PGID" ]; then
    groupmod -o -g "$PGID" ${RUN_AS};
fi

echo "
-------------------------------------
plex-f1-renamer will run as
-------------------------------------
User name:   ${RUN_AS}
User uid:    $(id -u ${RUN_AS})
User gid:    $(id -g ${RUN_AS})
-------------------------------------
"

export PUID
export PGID
export RUN_AS

exec /app/plex-f1-renamer
