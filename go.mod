module plex-f1-renamer

go 1.21

require (
	github.com/fsnotify/fsnotify v1.7.0
	github.com/go-test/deep v1.1.0
	go.uber.org/zap v1.27.0
)

require (
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
)
