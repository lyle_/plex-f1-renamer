FROM golang:latest as build

WORKDIR /usr/src/app
COPY go.mod go.sum *.go ./
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o plex-f1-renamer .


FROM debian:bullseye-slim

RUN \
  echo "**** install packages ****" && \
  apt-get update && \
  apt-get install -y \
    ca-certificates \
    passwd && \
  echo "**** add abc user ****" && \
  useradd --user-group --home-dir /data --shell /bin/false abc && \
  usermod --groups users abc && \
  echo "**** cleanup ****" && \
  apt-get autoremove && \
  apt-get clean

ENV PUID= \
    PGUID=

VOLUME /data

WORKDIR /app
COPY --chown=abc:users entrypoint.sh .
COPY --from=build --chown=abc:users /usr/src/app/plex-f1-renamer .

ENTRYPOINT ["/app/entrypoint.sh"]
